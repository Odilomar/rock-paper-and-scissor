import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BrainService {

  private humanHand: string = "";
  private cpuChoise: string = "";

  constructor() { }

  public getHumanHand(){
    return this.humanHand;
  }

  public setHumanHand(hand: string){
    this.humanHand = hand;
  }

  public clearHand() {
    this.humanHand = "";
  }

  public isEmpty(){
    return this.humanHand == "";
  }

  public getCPUChoise(){
    return this.chooseHand();
  }
  
  //TODO: Gerar um número aleatório entre 0 e 2 para definir a mão da CPU
  private chooseHand(){
    let allHands = ["paper", "rock", "scissors"];
    let handArrayPosition: number = 0;
    
    do {
      handArrayPosition = Math.floor(Math.random() * allHands.length)
    } while (handArrayPosition >= allHands.length && handArrayPosition < 0);

    return allHands[handArrayPosition];
  }
}
