import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { BrainService } from "../brain.service";

@Component({
  selector: "game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.css"],
})
export class GameComponent implements OnInit {
  @Output() isMenuEvent = new EventEmitter<boolean>();

  public yourHand: string;
  public divIdForHand: string;
  public btnIdForHand: string;

  private cpuHand: string;
  public divIdCpuHand: string;
  public btnIdCpuHand: string;

  constructor(private brainService: BrainService) {
    this.yourHand = "";
    this.divIdForHand = "";
    this.btnIdForHand = "";
    
    this.cpuHand = "";
    this.divIdCpuHand = "divCpuHand";
    this.btnIdCpuHand = "";
  }

  ngOnInit(): void {
    this.yourHand = this.brainService.getHumanHand();
    this.btnIdForHand = `btn${
      this.yourHand[0].toUpperCase() + this.yourHand.slice(1)
    }`;
    this.divIdForHand = `${this.btnIdForHand}-container`;

    this.cpuHand = this.brainService.getCPUChoise();
    this.btnIdCpuHand = `btn${
      this.cpuHand[0].toUpperCase() + this.cpuHand.slice(1)
    }`;
    this.divIdCpuHand = `${this.btnIdCpuHand}-container`;
  }

  public playAgain() {
    this.brainService.clearHand();
    this.isMenuEvent.emit(true);
  }
}
