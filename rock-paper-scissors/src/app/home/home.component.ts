import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BrainService } from '../brain.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @Output() isMenuEvent = new EventEmitter<boolean>();

  ngOnInit(): void {
  }

  constructor(
    private brainService: BrainService
  ) {}

  public chooseHand(hand: string){
    this.brainService.setHumanHand(hand);
    this.isMenuEvent.emit(false);
  }

}
